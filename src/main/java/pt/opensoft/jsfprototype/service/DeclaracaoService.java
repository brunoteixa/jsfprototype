package pt.opensoft.jsfprototype.service;

import org.springframework.stereotype.Service;
import pt.opensoft.jsfprototype.model.Declaracao;
import pt.opensoft.jsfprototype.model.ObjectFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Logger;

@Service("DeclaracaoService")
public class DeclaracaoService {

    private static final Logger logger = Logger.getLogger(DeclaracaoService.class.getSimpleName());

    public String submete(Declaracao declaracao) throws JAXBException {
        return valida(declaracao);

    }

    public String valida(Declaracao declaracao) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE );
        StringWriter writer = new StringWriter();
        jaxbMarshaller.marshal(declaracao, writer);
        logger.info(writer.toString());
        String error = null;

        if (declaracao.getAnexoA() != null && declaracao.getAnexoA().getQuadro1() != null && declaracao.getAnexoA().getQuadro1().getPercentagem() != null && !declaracao.getAnexoA().getQuadro1().getPercentagem().isEmpty()) {
            logger.info("percentagem está preenchida");
            if (declaracao.getAnexoA().getQuadro2() == null || declaracao.getAnexoA().getQuadro2().getValor() == null || declaracao.getAnexoA().getQuadro2().getValor().isEmpty()) {
                error = "Se a percentagem for preenchida, é necessário preencher o valor";
            } else {
                logger.info("valor: ->" + declaracao.getAnexoA().getQuadro2().getValor() + "<-");
            }
        }
        return error;
    }

    public Declaracao getPP() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        Declaracao declaracao = (Declaracao) jaxbUnmarshaller.unmarshal(Declaracao.class.getResourceAsStream("pre-preenchido.xml"));
        return declaracao;
    }

    public Declaracao getNova() throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        Declaracao declaracao = (Declaracao) jaxbUnmarshaller.unmarshal(Declaracao.class.getResourceAsStream("novo.xml"));
        return declaracao;

    }

}