package pt.opensoft.jsfprototype.managedController;
 
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import pt.opensoft.jsfprototype.model.Declaracao;
import pt.opensoft.jsfprototype.service.DeclaracaoService;

@ManagedBean(name="declaracaoMB")
@ViewScoped
public class DeclaracaoManagedBean implements Serializable {
 
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger((DeclaracaoManagedBean.class.getSimpleName()));

    @ManagedProperty(value="#{DeclaracaoService}")
    DeclaracaoService declaracaoService;

    private Declaracao declaracao;

    public DeclaracaoManagedBean() {
        logger.info("Foi contruído o  DeclaracaoManagedBean");
    }

    public void submete() {
        logger.info("Foi invocado o submete");
        try {
            String error = getDeclaracaoService().submete(declaracao);
            if (error != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", error));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "A declaração foi submetida com sucesso"));
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
 
    }

    public void valida() {
        logger.info("Foi invocado o valida");
        try {
            String error = getDeclaracaoService().valida(declaracao);
            if (error != null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", error));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "A declaração está correta"));
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal!", "System Error"));
        }
    }

    public DeclaracaoService getDeclaracaoService() {
        return declaracaoService;
    }

    public void setDeclaracaoService(DeclaracaoService declaracaoService) {
        this.declaracaoService = declaracaoService;
    }

    public Declaracao getDeclaracao() {
        logger.info("Foi invocado o getDeclaracao");
        return declaracao;
    }

    public String getLoadDeclaracao() throws JAXBException {
        logger.info("Foi invocado o loadDeclaracao");
        Map<String,String> params =
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String action = params.get("action");
        logger.info(action);
        if (action != null && action.equals("pp")) {
            declaracao = getDeclaracaoService().getPP();
        } else {
            declaracao = getDeclaracaoService().getNova();
        }

        return "";
    }

    public String redirect()  {
        logger.info("Foi invocado o redirect");
        Map<String,String> params =
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String action = params.get("action");
        logger.info(action);
        if (action != null && action.equals("pp")) {
            return "index";
        } else {
            return "index";
        }

    }

    public void removeAnexoA() {
        logger.info("Foi invocado o removeAnexoA");
        declaracao.setAnexoA(null);
    }

    public void criaAnexoA() {
        logger.info("Foi invocado o criaAnexoA");
        declaracao.setAnexoA(new Declaracao.AnexoA());
        declaracao.getAnexoA().setQuadro1(new Declaracao.AnexoA.Quadro1());
        declaracao.getAnexoA().setQuadro2(new Declaracao.AnexoA.Quadro2());
    }


    public void setRostoQuadro1DataRececao(Date dataRececao) throws DatatypeConfigurationException {
        logger.info("Foi invocado o setRostoQuadro1DataRececao");
        if (dataRececao == null) {
            declaracao.getRosto().getQuadro1().setDataRececao(null);
        } else {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            logger.info("data antes de colocar no xml: df.format(dataRececao)");
            declaracao.getRosto().getQuadro1().setDataRececao(df.format(dataRececao));
        }
    }

    public Date getRostoQuadro1DataRececao() throws ParseException {
        logger.info("Foi invocado o getRostoQuadro1DataRececao");
        Date result = null;
        if (declaracao.getRosto().getQuadro1().getDataRececao() != null && !declaracao.getRosto().getQuadro1().getDataRececao().isEmpty()) {
            logger.info("data como veio do xml: " + declaracao.getRosto().getQuadro1().getDataRececao());
            result = new SimpleDateFormat("yyyy-MM-dd").parse(declaracao.getRosto().getQuadro1().getDataRececao());
        }
        return result;
    }

    public Double getAnexoAQuadro2Valor() {
        logger.info("Foi invocado o getAnexoAQuadro2Valor");
        Double result = null;
        if (declaracao.getAnexoA().getQuadro2().getValor() != null && !declaracao.getAnexoA().getQuadro2().getValor().isEmpty()) {
            result = new Double(declaracao.getAnexoA().getQuadro2().getValor()) / 100;
        }
        return result;
    }

    public void setAnexoAQuadro2Valor(Double valor) {
        logger.info("Foi invocado o setRostoQuadro1DataRececao");
        if (valor == null || valor.equals(Double.parseDouble("0"))) {
            declaracao.getAnexoA().getQuadro2().setValor(null);
        } else {
            declaracao.getAnexoA().getQuadro2().setValor(((long) (valor * 100)) + "");
        }
    }
}